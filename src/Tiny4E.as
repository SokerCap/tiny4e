package
{
	import away3d.cameras.*;
	import away3d.containers.*;
	import away3d.controllers.*;
	import away3d.core.base.SubGeometry;
	import away3d.debug.*;
	import away3d.entities.Mesh;
	import away3d.events.LoaderEvent;
	import away3d.extrusions.*;
	import away3d.filters.*;
	import away3d.lights.*;
	import away3d.loaders.Loader3D;
	import away3d.loaders.parsers.Parsers;
	import away3d.materials.*;
	import away3d.materials.lightpickers.*;
	import away3d.materials.methods.*;
	import away3d.primitives.*;
	import away3d.textures.*;
	
	import com.tiny4e.*;
	import com.tiny4e.map.MapGenerator;
	import com.tiny4e.map.SpaceGenerator;
	
	import flash.display.*;
	import flash.events.*;
	import flash.filters.*;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.geom.Vector3D;
	import flash.net.URLRequest;
	import flash.text.*;
	import flash.ui.*;
	
	[SWF(backgroundColor="#000000", frameRate="60", quality="LOW",width="960",height="540")]
	public class Tiny4E extends Sprite
	{
		[Embed(source="../assets/texture/rockbase_diffuse.jpg")]
		private var IMG_ROCKBASE_DIFFUSE:Class;
		
		//engine variables
		private var scene:Scene3D;
		private var camera:Camera3D;
		private var view:View3D;
		private var cameraController:FirstPersonController;
		private var awayStats:AwayStats;
		private var _loader : Loader3D;
		
		//light objects
		//private var sunLight:DirectionalLight;
		private var sunLight:PointLight;
	
		private var lightPicker:StaticLightPicker;
		private var fogMethod:FogMethod;
		
		//material objects
		
		//scene objects
		private var text:TextField;
		private var tinyPlanet:Mesh;
		private var spaceSphere:Mesh;
		private var sun:Mesh;
		private var sunRadius:Number = 1000;
		private var sunSpeed:Number = 1.5;
		private var sunAngle:Number = 90;
		
		private var cubeGuys:Array;
		private var waveCycle:Number = 5;
		private var wave:Number = 0;
		public var threeSixtyRads:Number = 360 * (Math.PI / 180);
		private var mapGen:MapGenerator;
		private var rockBD:BitmapData;
		private var rockBT:BitmapTexture;
		private var rockMT:TextureMaterial;
		private var sky:Mesh;
		
		//rotation variables
		private var move:Boolean = false;
		private var lastPanAngle:Number;
		private var lastTiltAngle:Number;
		private var lastMouseX:Number;
		private var lastMouseY:Number;
	
		
		private var mDown:Boolean = false;
		private var mDownPos:Point= new Point();
		
		
		private var planetSize:Number = 400;
		private var spaceSphereSize:Number = 4000;
		private var scrollInterval:Number = 10;
		
		public function Tiny4E()
		{
			view = new View3D();
			view.backgroundColor = 0x000000;

			view.antiAlias = 4;
			
			this.addChild(view);

			this.addEventListener(Event.ENTER_FRAME, onEnterFrame);
			
			Parsers.enableAllBundled();
			
			initEngine();
			
			
			this.initPlanet();
			this.initSpace();

			initLights();
			initText();
			initListeners();
		}
		
		private function initEngine():void
		{
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;
			
			scene = view.scene;
			camera = view.camera;
			
			
			
			_loader = new Loader3D();
			
			camera.lens.far = 14000;
			camera.lens.near = .05;
			camera.y = 0;
			camera.z = 800;
			
			//setup controller to be used on the camera
			cameraController = new FirstPersonController(camera, 180, 0, -80, 80);
			
			view.addSourceURL("srcview/index.html");
			addChild(view);
			
			
			awayStats = new AwayStats(view)
			addChild(awayStats);
		}
		
		private function initText():void
		{
			text = new TextField();
			text.defaultTextFormat = new TextFormat("Verdana", 11, 0xFFFFFF);
			text.width = 240;
			text.height = 100;
			text.selectable = false;
			text.mouseEnabled = false;
			text.text = "";
			
			text.filters = [new DropShadowFilter(1, 45, 0x0, 1, 0, 0)];
			
			addChild(text);
		}
		
		private function initLights():void
		{
			//sunLight = new DirectionalLight(1000, 1000, -100);
			sunLight = new PointLight();
			//sunLight.radius = this.sunRadius;
			sunLight.color = 0xff0000;
			//sunLight.ambient = 0.1;
			sunLight.diffuse = .1;
			sunLight.castsShadows = true;
			sunLight.specular = 1;
			sunLight.radius = 100; 
			sunLight.fallOff = 1000;
			sunLight.ambient = 0xa0a0c0;
			sunLight.ambient = .5; 
			//sunLight.position = new Vector3D(1000,1000,-1000);
			sunLight.y = 2000; 
			//sunLight.lookAt(tinyPlanet.position);
			scene.addChild(sunLight);
			
			lightPicker = new StaticLightPicker([sunLight]);
			
			
			//create a global fog method
			//fogMethod = new FogMethod(0, 8000, 0xcfd9de);
		}
		
		private function initMaterials():void {
			//generate planet texture ahaha
		}
		
		private function initSpace():void {
			var spaceGen:SpaceGenerator = new SpaceGenerator();
			var geom:SphereGeometry = new SphereGeometry(10000,32,24);
			var material:TextureMaterial = new TextureMaterial(new BitmapTexture(spaceGen.generate(1024,1024)));
			material.bothSides = true;
			spaceSphere = new Mesh(geom,material);
			scene.addChild(spaceSphere);
		}
		private function initPlanet():void {
			var textW:Number = 512;
			var textH:Number = textW;
			var iceHeight:Number = 60;
			var geom:SphereGeometry = new SphereGeometry(planetSize,32,24);
			rockBD = new BitmapData(textW,textW,false,0xff0000ff);
			rockBD.perlinNoise(100,100,10,500,true,true, BitmapDataChannel.BLUE);
			//rockBD.fillRect(new Rectangle(50,0,textW-50,iceHeight), 0xaadddddd);
			mapGen = new MapGenerator();
			rockBD = mapGen.genLand(rockBD);
			rockBT = new BitmapTexture(rockBD);
			//var bmT:BitmapTexture = new BitmapTexture(rockBD);
			
			
			rockMT = new TextureMaterial(rockBT);
			//var sMaterial:S
			rockMT.specular = .5;
			rockMT.gloss = 50;
			rockMT.ambientColor = 0xff0000;
			rockMT.lightPicker = lightPicker;
			rockMT.bothSides = false;
			//material.shadowMethod = 
			
			
			tinyPlanet = new Mesh(geom,rockMT);
			
			var skyBD:BitmapData = new BitmapData(textW,textW,true,0x22ff00ff);
			//skyBD.noise(500,100,155);
			skyBD.perlinNoise(264,100,6,400,true,true,BitmapDataChannel.ALPHA|BitmapDataChannel.BLUE);
			var skyBT:BitmapTexture = new BitmapTexture(skyBD);
			var skyMT:TextureMaterial = new TextureMaterial(skyBT);
			skyMT.alphaBlending = true; 
			//skyMT.alpha = 0.5; skyMT.lightPicker = lightPicker; skyMT.specular = 0;
			var geomS:SphereGeometry = new SphereGeometry(planetSize+20,32,24);
			sky = new Mesh( geomS, skyMT );
			
			//tinyPlanet.addChild(sky);
			
			scene.addChild(tinyPlanet);
			tinyPlanet.addChild(sky);
			
			tinyPlanet.position = new Vector3D(0,0,0);
			
			
			
			camera.lookAt(tinyPlanet.position);
			
			//make a sun!
			var sunBD:BitmapData = new BitmapData(256,256,false,0xffFFFF99);
			//sunBD.perlinNoise(2,4,8,500,true,true);
			var sunMat:TextureMaterial = new TextureMaterial(new BitmapTexture(sunBD));
			 sunMat.specular = .6; sunMat.ambientColor = 0xff0000; sunMat.gloss = 500;
			 sunMat.lightPicker = lightPicker;
			var sunGeo:SphereGeometry = new SphereGeometry(100);
			sun = new Mesh(sunGeo,sunMat);
			sun.position = new Vector3D(1000,1000,0);
			tinyPlanet.addChild(sun);

			
			cubeGuys = new Array();
		}
		
		
		
		
		/**
		 * Initialise the listeners
		 */
		private function initListeners():void
		{
			//addEventListener(Event.ENTER_FRAME, onEnterFrame);
			stage.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			stage.addEventListener(MouseEvent.MOUSE_WHEEL, onMouseWheel);
			stage.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
			stage.addEventListener(Event.RESIZE, onResize);
			stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
			stage.addEventListener(KeyboardEvent.KEY_UP, onKeyUp);
			onResize();
		}
		
		private function onResourceComplete(ev : LoaderEvent) : void
		{
			_loader.removeEventListener(LoaderEvent.RESOURCE_COMPLETE, onResourceComplete);
			_loader.removeEventListener(LoaderEvent.LOAD_ERROR, onLoadError);
			view.scene.addChild(_loader);
		}
		private function onLoadError(ev : LoaderEvent) : void
		{
			trace('Could not find', ev.url);
			_loader.removeEventListener(LoaderEvent.RESOURCE_COMPLETE, onResourceComplete);
			_loader.removeEventListener(LoaderEvent.LOAD_ERROR, onLoadError);
			_loader = null;
		}
		private function rotate(xDiff:Number,yDiff:Number):void 
		{
			tinyPlanet.rotationX += xDiff;
			tinyPlanet.rotationY += yDiff;
			spaceSphere.rotationX += xDiff;
			spaceSphere.rotationY += yDiff;
		}
		public function onEnterFrame(ev:Event):void {
			
			if (move) {
				//cameraController.panAngle = 0.3*(stage.mouseX - lastMouseX) + lastPanAngle;h
				//cameraController.tiltAngle = 0.3*(stage.mouseY - lastMouseY) + lastTiltAngle;
				
				var mdiff:Point = new Point(stage.mouseX,stage.mouseY).subtract(mDownPos);
				this.rotate(mdiff.y/30,mdiff.x/30);
				
				//move sun orbit
				//this.sunAngle = tinyPlanet.rotationX;
				//this.sunAngle %= 360;
				//var rad:Number = this.sunAngle * (Math.PI / 180);
				//var newXY:Point = new Point( this.sunRadius*Math.cos(rad), this.sunRadius*Math.sin(rad) );
				//sun.x = newXY.x; sun.y = newXY.y;
				//var lightAngle:Number = sunAngle - 90;
				//lightAngle %= 360;
				//rad = lightAngle * (Math.PI / 180);
//				//sunLight.x = 1200*Math.cos(rad); sunLight.y = 1200*Math.sin(rad);
				//sunLight.rotateTo(tinyPlanet.rotationX,0,0);
				//trace("Light moved to "+sunLight.x+","+sunLight.y);
				//sunLight.lookAt(tinyPlanet.position);
				//sunLight.position = sun.position;
				//sunLight.transform = sun.transform.clone();
				
				if(cubeGuys.length > 0){
					//rotate all entities same as planet
					for(var i:int=0;i<cubeGuys.length;i++){
						cubeGuys[i][1] += mdiff.x / 200;
						var cubeAng:Number = cubeGuys[i][1];
						cubeAng %= 360;
						cubeGuys[i][2] += mdiff.y / 200;
						var yAng:Number = cubeGuys[i][2]; 
						yAng %= 360; 
						yAng = yAng * (Math.PI / 180);
						cubeAng = cubeAng * (Math.PI/ 180);
						//(cubeGuys[i][0] as Mesh).x = (this.planetSize-20) * Math.cos(cubeAng);
						//(cubeGuys[i][0] as Mesh).z = (this.planetSize-20) * Math.sin(cubeAng);
						//(cubeGuys[i] as Mesh).z = (this.planetSize+40) * Math.tan(rad);
						
						//(cubeGuys[i][0] as Mesh).y = (this.planetSize-20) * Math.sin(yAng);
						//(cubeGuys[i][0] as Mesh).z = (this.planetSize-20) * Math.cos(yAng); 
						/*// rotation around x axis (a.k.a. pitch)
						(cubeGuys[i][0] as Mesh).y = Math.cos(cubeAng)*(cubeGuys[i][0] as Mesh).y - Math.sin(cubeAng)*(cubeGuys[i][0] as Mesh).z;
						(cubeGuys[i][0] as Mesh).z = Math.sin(cubeAng)*(cubeGuys[i][0] as Mesh).y + Math.cos(cubeAng)*(cubeGuys[i][0] as Mesh).z;
						
						// rotation around y axis (a.k.a. yaw)
						(cubeGuys[i][0] as Mesh).z = Math.cos(yAng)*(cubeGuys[i][0] as Mesh).z - Math.sin(yAng)*(cubeGuys[i][0] as Mesh).x;
						(cubeGuys[i][0] as Mesh).x = Math.sin(yAng)*(cubeGuys[i][0] as Mesh).z + Math.cos(yAng)*(cubeGuys[i][0] as Mesh).x;
						
						// rotation around z axis (a.k.a. roll)
						//(cubeGuys[i][0] as Mesh).x = Math.cos(angleAroundZAxis)*(cubeGuys[i][0] as Mesh).x - Math.sin(angleAroundZAxis)*(cubeGuys[i][0] as Mesh).y;
						//(cubeGuys[i][0] as Mesh).y = Math.sin(angleAroundZAxis)*(cubeGuys[i][0] as Mesh).x + Math.cos(angleAroundZAxis)*(cubeGuys[i][0] as Mesh).y;
						*/
						}
					
					
					//tinyPlanet.material.lightPicker = lightPicker;
					
				}
				//rockBD = mapGen.updateBitmap(rockBD);
				//rockBD.fillRect( new Rectangle( 0, 0, 260, 260), 0xffffff);
				//tinyPlanet.material = new TextureMaterial(new BitmapTexture(rockBD));
				
				
			}
			
			
			sky.rotationY += 0.5;
			if(camera.z != 0 ) camera.lookAt(tinyPlanet.position);
		
			text.text = "Camera at "+camera.position.toString()+" Planet:"+tinyPlanet.position.toString();
			
			view.render();
		}
		private function onKeyDown(event:KeyboardEvent):void
		{
			if(event.keyCode == Keyboard.C){
				//add cube guy 
				var guyG:CubeGeometry = new CubeGeometry(40,40,40);
				var guyM:ColorMaterial = new ColorMaterial(0xdd3333);
				guyM.lightPicker = lightPicker;
				guyM.specular = 0.5
				var guy:Mesh = new Mesh(guyG,guyM);
				
				guy.x = 0; guy.z = planetSize+20;
				guy.castsShadows = true;
				this.cubeGuys.push([guy,0,0]);
				//scene.addChild(guy);
				tinyPlanet.addChild(guy);
				trace("CubeGuy Added");
				trace(guy.position);
				mapGen.addCubeGuy();
			}
			else if(event.keyCode == Keyboard.X){
				var newCone:ConeGeometry = new ConeGeometry(10,50);
				var sub:SubGeometry = new SubGeometry();
				
				//tinyPlanet.geometry.addSubGeometry( sub);
				var v:Vector.<Number> = SubGeometry(tinyPlanet.geometry.subGeometries[0]).vertexData;
				var i:int;
				// manipulate random vertices to deform sphere
				for (var whut:int = 1; whut < 8; whut += 1) { //deform 10 vertices
					i = Math.floor(Math.random()*(v.length/3) );
					i *= 3;
					var vec:Vector3D = new Vector3D(v[i],v[i+1],v[i+2]);
					//vec = vec* (Math.random()*2);
					vec.scaleBy( (Math.random()/5 + 0.9) );
					v[i] = vec.x; v[i+1] = vec.y; v[i+2] = vec.z;
					// multiply by the sine of the x coordinate, plus offset for animation (normalized to be 0-360 degrees)
					//v[i] = Math.sin(((v[i - 2] + waveCycle) / 100) * (threeSixtyRads * 1) )  * 2;
					// add harmonic from Y frequency
					//v[i] += Math.sin(((v[i - 1] + waveCycle) / 100) * (threeSixtyRads * 1) )  * 1;
					//var dv:Number = Math.random()*10-20;
					//v[i] += dv; v[i+1] += dv; v[i+2] += dv;
				}
				SubGeometry(tinyPlanet.geometry.subGeometries[0].updateVertexData(v));
				
			}
		}
		
		private function onKeyUp(event:KeyboardEvent):void
		{
			
		}
		
		private function onMouseDown(event:MouseEvent):void
		{
			
			move = true;
			lastPanAngle = cameraController.panAngle;
			lastTiltAngle = cameraController.tiltAngle;
			lastMouseX = stage.mouseX;
			lastMouseY = stage.mouseY;
			stage.addEventListener(Event.MOUSE_LEAVE, onStageMouseLeave);
			mDownPos = new Point(lastMouseX,lastMouseY);
			trace("MOUSE DOWN");
		}
		private function onMouseWheel(event:MouseEvent):void
		{
			if (event.delta < 0 && this.camera.z < this.spaceSphereSize + this.scrollInterval) {
				this.camera.z += this.scrollInterval;
			}
			else if (this.camera.z > this.planetSize + this.scrollInterval) {
				this.camera.z -= this.scrollInterval;
			}
		}
		private function onMouseUp(event:MouseEvent):void
		{
			move = false;
			stage.removeEventListener(Event.MOUSE_LEAVE, onStageMouseLeave);
		}
		private function onStageMouseLeave(event:Event):void
		{
			move = false;
			stage.removeEventListener(Event.MOUSE_LEAVE, onStageMouseLeave);
		}
		private function onResize(event:Event = null):void
		{
			view.width = stage.stageWidth;
			view.height = stage.stageHeight;
			awayStats.x = stage.stageWidth - awayStats.width;
		}
	}
}
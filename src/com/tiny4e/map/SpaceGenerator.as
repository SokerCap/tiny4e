package com.tiny4e.map
	
{
	import flash.display.BitmapData;
	import flash.geom.ColorTransform;

	public class SpaceGenerator
	{
		public function generate(width:Number,height:Number):BitmapData
		{
			var space:BitmapData = new BitmapData(width, height, false);
			space.noise(Math.random() * 256, 0, 255, 7, true);
			var a:Number = 20.4;
			var b:Number = 0xFF * -20;
			space.colorTransform(space.rect, new ColorTransform(a,a,a,1,b,b,b));
			return space;
		}
	}
}
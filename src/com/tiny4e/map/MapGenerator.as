package com.tiny4e.map
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	public class MapGenerator
	{
		private const WATER:int=0;
		private const LAND:int=1;
		private var iterations:int=5;
		private var simultaneously:Boolean=false;
		private var landRatio:Number=0.50;
		private var mapWidth:int=512;
		private var mapHeight:int=512;
		private var tileSize:int=2;
		private var canvas:Sprite;
		private var landArray:Array;
		private var newLandArray:Array;
		private var bd:BitmapData;
		private var cubeGuys:Array = new Array();
		private var cubGuyW:int = 5;
		
		public function MapGenerator()
		{
			
		}
		
		public function genLand(bm:BitmapData):BitmapData {
			bd = bm;
			landArray = new Array();
			newLandArray = new Array();
			generateMap();
			return bd;
		}
		
		private function generateMap():void {
			landOrWater();
			drawMap();
		}
		private function drawMap():void {
			for (var i:int=0; i<mapWidth; i++) {
				for (var j:int=0; j<=mapWidth; j++) {
					switch (landArray[i][j]) {
						case WATER :
							drawWater(i,j);
							break;
						case LAND :
							drawLand(i,j);
							break;
					}
				}
			}
		}
		private function landOrWater():void {
			for (var i:int=0; i<mapWidth; i++) {
				landArray[i]=new Array();
				newLandArray[i]=new Array();
				for (var j:int=0; j<=mapWidth; j++) {
					if (Math.random()<landRatio) {
						landArray[i][j]=LAND;
					} else {
						landArray[i][j]=WATER;
					}
				}
			}
			for (i=1; i<=iterations; i++) {
				iterateLand(LAND,WATER);
			}
		}
		
		public function addCubeGuy(x:Number=-1,y:Number=-1):void {
			//add random cube guy
			if(x<0){
				x = Math.floor(Math.random()*mapWidth);
			}
			if(y<0){
				y = Math.floor(Math.random()*mapWidth);
			}
			
			//iterate x and y until it hits land then use that for coordinate
			while( landArray[x][y] != LAND){
				x += Math.floor(Math.random()*5)+1;
				y += Math.floor(Math.random()*5)+1;
				if(x >= mapWidth) x = mapWidth-1;
				if(y >= mapWidth) y= mapWidth-1;
			}
			var guy:Point = new Point(x,y);
			trace("drawing guy at "+x+","+y);
			cubeGuys.push(guy);
		}
		
		public function updateBitmap(_bd:BitmapData):BitmapData {
			bd = _bd;
			for(var i:int=0;i<cubeGuys.length;i++){
				var guy:Point = (cubeGuys[i] as Point);
				if( landArray[guy.x][guy.y] == LAND){
					drawLand(guy.x,guy.y);
				}
				else {
					bd.fillRect( new Rectangle(tileSize*guy.x,tileSize*guy.y,tileSize,tileSize), 0x00000000);
				}
				var newXY:Point = new Point(guy.x,guy.y);
				newXY.x += Math.floor(Math.random()*tileSize+1);
				newXY.y += Math.floor(Math.random()*tileSize+1);
				while(landArray[newXY.x][newXY.y] != LAND ){
					newXY.x = guy.x + Math.floor(Math.random()*(tileSize)+1)-tileSize;
					newXY.y = guy.y + Math.floor(Math.random()*(tileSize)+1)-tileSize;
				}
				(cubeGuys[i] as Point).x = newXY.x;
				(cubeGuys[i] as Point).x = newXY.y;
				drawLand(newXY.x,newXY.y);
			}
			return bd;
		}
		
		private function drawLand(col:int,row:int):void {
			bd.fillRect( new Rectangle(tileSize*col,tileSize*row,tileSize,tileSize), 0x00ff00);
			
			/*canvas.graphics.beginFill(0x00ff00);
			canvas.graphics.lineStyle(1,0x000000);
			canvas.graphics.drawRect(tileSize*col,tileSize*row,tileSize,tileSize);
			canvas.graphics.endFill(); */
		}
		private function drawWater(col:int,row:int):void {
			
		}
		private function iterateLand(land:int,defaultLand):void {
			for (var i:int=0; i<mapWidth; i++) {
				for (var j:int=0; j<=mapWidth; j++) {
					if (landArray[i][j]==land||landArray[i][j]==defaultLand) {
						if (adjacentLand(i,j,land)>=5) {
							if (! simultaneously) {
								landArray[i][j]=land;
							} else {
								newLandArray[i][j]=land;
							}
						} else {
							if (! simultaneously) {
								landArray[i][j]=defaultLand;
							} else {
								newLandArray[i][j]=defaultLand;
							}
						}
					}
				}
			}
			if (simultaneously) {
				for (i=0; i<mapWidth; i++) {
					for (j=0; j<=mapWidth; j++) {
						landArray[i][j]=newLandArray[i][j];
					}
				}
			}
		}
		private function adjacentLand(col:int,row:int,lookFor:int):int {
			var found:int=0;
			for (var i:int=-1; i<=1; i++) {
				for (var j:int=-1; j<=1; j++) {
					if (landArray[col+i]!=undefined&&landArray[col+i][row+j]==lookFor) {
						found++;
					}
				}
			}
			return found;
		}
	}
}